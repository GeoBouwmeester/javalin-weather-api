package hu.adsd;

public class WeatherInfo {
    private String city;
    private int temperature;

    public WeatherInfo(String city, int temperature) {
        this.city = city;
        this.temperature = temperature;
    }

    public String getCity() {
       return this.city;
    }

    public int getTemperature() {
        return this.temperature;
    }
}
